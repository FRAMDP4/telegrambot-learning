'use strict'

const Telegram = require('telegram-node-bot')
//const tg = new Telegram.Telegram('236943988:AAGzPviSP5Cj0MnU_90aOs5F_WCSOioOvIw') //token harry
const tg = new Telegram.Telegram('254186472:AAHuGjfiimGyDKcVuPijusLseN_cko9P1Qo') // token fer
// const tg = new Telegram.Telegram('275012770:AAElKfnlPgXczxcxjNSz-K9ZvwO6Kl98D_4') //token REAL
//const tg = new Telegram.Telegram('250918047:AAHwKk6sOlTMeZNsA5nqxx-3cIK2fQLN4Fw') // token juju
//const tg = new Telegram.Telegram('260144141:AAFe2ztIC2kG3K3mB-MDJrHfPtTeDPIZtUA') //token Ivy
//const tg = new Telegram.Telegram('207783384:AAGwLlGZOGZrC0j0AyCktVFQc2MPAkCWi2I') //Fanny
const PingController = require('./controller/PingController.js').PingController
const OtherwiseController = require('./controller/OtherwiseController.js').OtherwiseController
const StartController = require('./controller/StartController.js').StartController
const HelpController = require('./controller/HelpController.js').HelpController
const DashboardController = require('./controller/DashboardController.js').DashboardController
const NotifierController = require('./controller/NotifierController.js').NotifierController
const MessageController = require('./controller/MessageController.js').MessageController
const ForumController = require('./controller/ForumController.js').ForumController
const ResetController = require('./controller/ResetController.js').ResetController
const FinanceController = require('./controller/FinanceController.js').FinanceController
const GslcController = require('./controller/GslcController.js').GslcController

var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var fs = require("fs");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

tg.router
    .when(['ping'], new PingController())
    .when(['start'], new StartController())
    .when(['help'], new HelpController())
    .when(['course','today','weekly','news'] , new DashboardController())
    .when(['message'], new MessageController())
    .when(['forum'], new ForumController())
    .when(['finance'], new FinanceController())
    .when(['reset'], new ResetController())
    .when(['gslc'], new GslcController())
    //.when(['broadcast'], new NotifierController(tg))
    .otherwise(new OtherwiseController())

app.post('/broadcast', function (req, res) {
   var msg = req.body.msg;
   res.send({success: true});
   var notifier = new NotifierController(tg, msg);
   notifier.run();
})

var server = app.listen(8088, function () {
  console.log('listening');
})