'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
//const strsql = "Driver={SQL Server Native Client 10.0};Server=10.200.200.130\\SQL2005;Database=BSISDB;Uid=BinusMobileAdministrator;Pwd=B!nusMobile123";
//this.CONNECTION_STRING = strsql;
//this.CONNECTION_STRING = "Driver={SQL Server Native Client 10.0};Server=10.200.200.238\\SQL2005;Database=BSISDB;Uid=bm3Dev;Pwd=Development3";
this.CONNECTION_STRING_TELEGRAM = "mssql://BinusMobileAdministrator:B!nusMobile123@10.200.200.130/TELEGRAM_DB";
this.CONNECTION_STRING_BM = "mssql://app_telegram:tele123@10.21.51.10/LMS_DB";
this.CONNECTION_STRING_MESSAGE_BM = "mssql://bm3dev:Development3@10.200.200.238/MESSAGE_DB";
this.CONNECTION_STRING_FORUM_BM = "mssql://bm3dev:Development3@10.200.200.238/FORUM_DB";

class Util {

    cek($, successCallback, failedCallback) {
        sql.connect("mssql://BinusMobileAdministrator:B!nusMobile123@10.200.200.130/TELEGRAM_DB").then(function() {
            new sql.Request()
            .input('TelegramID' , sql.Int , $._message._from._id)
            .execute('bn_Telegram_CheckUser').then(function(recordset){
                console.log(recordset[0]);
                recordset = recordset[0];
                if(recordset.length > 0){
                    if(recordset[0].PhoneNumber == null){
                        console.log('NOMOR TELP');
                        $.runMenu({
                            message: 'Please send your phone number (Use the button provided): ',
                            options: {
                                parse_mode: 'Markdown',
                                requestContact:true
                            },
                            oneTimeKeyboard:true,
                            'Send My Phone Number': ($) => {
                                console.log($);
                            },
                            'anyMatch': ($) => {
                                //console.log($);
                                new sql.Request()
                                .input('TelegramID' , sql.Int , $._message._from._id)
                                .input('PhoneNumber' , sql.VarChar(50) , $._message._contact._phoneNumber)
                                .execute('bn_Telegram_UpdatePhoneNumber').then(function(recordsets){
                                    $.sendMessage("Your phone number saved", true);
                                    $.sendMessage("Please Update Your NIM : ", true);
                                }).catch(function(err) {
                                    console.log(err);
                                    $.sendMessage("failed to save your phone number", true);
                                });
                            }
                        })
                        failedCallback();
                        return;
                    }
                    if(recordset[0].BinusianID == null){
                        $.sendMessage("Please Update Your NIM : ", true);
                        failedCallback();
                        return;
                    }
                    if(recordset[0].ChatID == null || recordset[0].ChatID != $._message._chat._id) {
                        new sql.Request()
                        .input('TelegramID' , sql.Int , $._message._from._id)
                        .input('ChatID' , sql.VarChar(20) , $._message._chat._id)
                        .execute('bn_Telegram_UpdateChatID');
                    }
                    if(recordset[0].OTP != null)
                    {
                        $.sendMessage("Please send OTP Code you get from Binusmaya (Menu -> Registration -> Telegram Registration", true);
                        failedCallback();
                        return;
                    }

                    var command = $._message._text;

                    if (command.indexOf("/reset") == 0) {
                        command = "/reset";
                    } else if (command.indexOf("/ping") == 0) {
                        command = "/ping";
                    } else if (command.indexOf("/start") == 0) {
                        command = "/start";
                    } else if (command.indexOf("/help") == 0) {
                        command = "/help";
                    } else if (command.indexOf("/course") == 0) {
                        command = "/course";
                    } else if (command.indexOf("/today") == 0) {
                        command = "/today";
                    } else if (command.indexOf("/weekly") == 0) {
                        command = "/weekly";
                    } else if (command.indexOf("/news") == 0) {
                        command = "/news";
                    } else if (command.indexOf("/message") == 0) {
                        command = "/message";
                    } else if (command.indexOf("/forum") == 0) {
                        command = "/forum";
                    } else if (command.indexOf("/finance") == 0) {
                        command = "/finance";
                    } else if (command.indexOf("/gslc") == 0) {
                        command = "/gslc";
                    }

                    new sql.Request()
                    .input('TelegramID' , sql.Int , $._message._from._id)
                    .input('Query' , sql.VarChar(150) , command)
                    .execute('bn_Telegram_InsertLog');
                    successCallback(recordset[0]);

                    return;
                }else{
                    console.log('New user registering bawah');
                    $.runMenu({
                        message: 'Please send your phone number (Use the button provided):',
                        options: {
                            parse_mode: 'Markdown',
                            requestContact:true
                        },
                        oneTimeKeyboard:true,
                        'Send My Phone Number': ($) => {
                            console.log($);
                            console.log('atas')
                        },
                        'anyMatch': ($) => {
                            console.log('bawah')
                            console.log($);
                            new sql.Request()
                            .input('TelegramID' , sql.Int , $._message._from._id)
                            .input('FirstName' , sql.VarChar(500) , $._message._from._firstName)
                            .input('LastName' , sql.VarChar(500) , $._message._from._lastName)
                            .input('Username' , sql.VarChar(50) , $._message._from._username)
                            .input('PhoneNumber' , sql.VarChar(50) , $._message._contact._phoneNumber)
                            .input('ChatID' , sql.VarChar(20) , $._message._chat._id)
                            .execute('bn_Telegram_Register');
                            $.sendMessage("Thank you, now please input your NIM", true);
                        }
                    })
                    failedCallback();
                    return;
                }
            }).catch(function(err) {
                console.log(err);
                failedCallback()
            }); 
        }).catch(function(err) {
            console.log(err);
            failedCallback()
        });
    };
}

exports.Util = Util