'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
const Util = require('../Util.js');

class OtherwiseController extends TelegramBaseController {
    handle($) {
        console.log($._message._text);
        // if(!$._message._text.length == 10 || !parseInt($._message._text)) {
        //     $.sendMessage('Invalid Command, please check your command and try again');
        //     return;
        // }
        //cek itu format nim atau bukan
        //cek dulu udah ada nim atau belum?
        //kalau belum ada nim jalanin ini
        //kalau udah ada kasih tau kalau dia salah command
        sql.connect(Util.CONNECTION_STRING_TELEGRAM).then(function() {
            new sql.Request()
            .input('TelegramID' , sql.Int , $._message._from._id)
            .execute('bn_Telegram_CheckUser').then(function(recordset){
                console.log(recordset);
                recordset = recordset[0];
                if(recordset[0] == null) {
                    $.runMenu({
                        message: 'Please send your phone number (Use the button provided):',
                        options: {
                            parse_mode: 'Markdown',
                            requestContact:true
                        },
                        oneTimeKeyboard:true,
                        'Send My Phone Number': ($) => {
                            console.log($);
                        },
                        'anyMatch': ($) => {
                            //console.log($);
                            new sql.Request()
                            .input('TelegramID' , sql.Int , $._message._from._id)
                            .input('FirstName' , sql.VarChar(500) , $._message._from._firstName)
                            .input('LastName' , sql.VarChar(500) , $._message._from._lastName)
                            .input('Username' , sql.VarChar(50) , $._message._from._username)
                            .input('PhoneNumber' , sql.VarChar(50) , $._message._contact._phoneNumber)
                            .input('ChatID' , sql.VarChar(20) , $._message._chat._id)
                            .execute('bn_Telegram_Register');
                            $.sendMessage("Thank you, now please input your NIM", true);
                        }
                    })
                }
                else if(recordset[0].BinusianID == null){
                    if($._message._text.length == 10 && parseInt($._message._text)){
                        new sql.Request()
                        .input('TelegramID' , sql.Int, $._message._from._id)
                        .input('NIM' , sql.VarChar(11), $._message._text)
                        .execute('bn_Telegram_UpdateNIM').then(function(recordsets){
                            $.sendMessage("Your NIM Saved! Please input OTP Code you get from binusmaya or send /reset if the NIM you have sent is wrong");
                        }).catch(function(err) {
                            console.log(err);
                        });
                    }
                    else
                        $.sendMessage('Wrong NIM Format, please check your NIM and try again');
                }
                else if(recordset[0].OTP != null)
                {
                    if(parseInt($._message._text) == recordset[0].OTP)
                    {
                        new sql.Request()
                        .input('TelegramID' , sql.Int, $._message._from._id)
                        .input('OTP' , sql.VarChar(11), $._message._text)
                        .execute('bn_Telegram_ValidateOTP').then(function(recordsets){
                            $.sendMessage("Your NIM Validated! Send /help to get started.");
                        }).catch(function(err) {
                            console.log(err);
                        });
                    }
                    else
                        $.sendMessage('Invalid OTP Code, please check your OTP Code and try again. Get it from Binusmaya! (Menu -> Registration -> Telegram Registration)');
                }
                else
                    $.sendMessage('Unrecognized command! Send /help to see available commands.');
            }).catch(function(err) {
                console.log(err);
            });
        }).catch(function(err) {
            console.log(err);
        });

        
    }
}

exports.OtherwiseController = OtherwiseController;