'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const Util = require('../Util.js');

class HelpController extends TelegramBaseController {
    helpHandler($) {
        $.sendMessage("<b>BeeBot</b>\n\nBinusian Bot to help Binusian access information that useful and relevant to them such as schedule, news, message, and many more! Don’t forget to send us your feedback.\n\n/help : See what bot can do\n/start : Start using Binusian Bot\n/reset : Reset your telegram registration data\n/course : Get your course list for active semester\n/today : Get your schedule for today\n/weekly : Get your weekly schedule\n/gslc : Get next 3 GSLC Schedules\n/forum : Get latest forum threads\n/message : Get latest message\n/news : Get latest news\n/finance : Get your financial report and status\n", 
        	{ parse_mode: 'HTML' })

    }
    get routes() {
        return {
            'help': 'helpHandler',
        }
    }
}

exports.HelpController = HelpController;