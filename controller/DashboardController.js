'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
const Util = require('../Util.js');


class DashboardController extends TelegramBaseController {
	courseHandler($){
		var u = new Util.Util()

		u.cek($, function(user) {
		 	sql.connect(Util.CONNECTION_STRING_BM).then(function() {
		 		new sql.Request()
		 		.input('UserID' , sql.VarChar(11) , user.EmployeeID)
				.execute('bn_Student_GetActiveAcadCareer').then(function(recordsets){
					recordsets = recordsets[0];
					var ACAD_CAREER = recordsets[0] == null ? 'RS1' : recordsets[0].ACAD_CAREER;

					new sql.Request()
					.input('NIM' , sql.VarChar(10) , user.EmployeeID)
					.input('ACAD' , sql.VarChar(4) , ACAD_CAREER)
					.execute('bn_Student_GetCourses').then(function(recordsets){
						recordsets = recordsets[0];
						if(recordsets.length > 0){
							$.sendMessage('List of Active Course : ')
							var courses = '';
							for(var i = 0; i < recordsets.length; i++){
								courses += (recordsets[i].COURSEID + ' - ' + recordsets[i].COURSENAME + ' - ' + recordsets[i].CLASS_SECTION)+"\n";
							}
							$.sendMessage(courses);

						}else{
							$.sendMessage('No Course Available')
						}

					}).catch(function(err){
						console.log(err)
					});	
				}).catch(function(err){
					console.log(err)
				});	
		 	});
		}, function(){
			console.log("failed in checking");
		})
		
	}

	gpaHandler($){
		var u = new Util.Util()

		u.cek($, function(user) {
		 	sql.connect(Util.CONNECTION_STRING_BM).then(function() {
		 		new sql.Request()
		 		.input('UserID' , sql.VarChar(10) , user.EmployeeID)
				.execute('bn_Student_GetActiveAcadCareer').then(function(recordsets){
					recordsets = recordsets[0];

					var ACAD_CAREER = recordsets[0] == null ? 'RS1' : recordsets[0].ACAD_CAREER;

					new sql.Request()
					.input('StudentID' , sql.VarChar(10) , user.BinusianID)
					.input('ACAD_CAREER' , sql.VarChar(4) , ACAD_CAREER)
					.execute('bn_Student_GetGPA').then(function(recordsets){
						recordsets = recordsets[0];
						if(recordsets.length > 0){

							$.sendMessage('Your GPA : ' + recordsets[recordsets.length-1].GPA);
						}else{
							$.sendMessage('GPA Not Available')
						}

					}).catch(function(err){
						console.log(err)
					});	
				}).catch(function(err){
					console.log(err)
				});	
		 	});
		}, function(){
			console.log("failed in checking");
		})
	}
		
	todayHandler($){
	var u = new Util.Util()

		u.cek($, function(user) {
		 	sql.connect(Util.CONNECTION_STRING_BM).then(function() {
		 		new sql.Request()
		 		.input('UserID' , sql.VarChar(10) , user.EmployeeID)
				.execute('bn_Student_GetActiveAcadCareer').then(function(recordsets){
					recordsets = recordsets[0];
					var ACAD_CAREER = recordsets[0] == null ? 'RS1' : recordsets[0].ACAD_CAREER;

					new sql.Request()
					.input('StudentID' , sql.VarChar(10) , user.BinusianID)
					.input('ACAD_CAREER' , sql.VarChar(4) , ACAD_CAREER)
					.execute('bn_Student_GetClassScheduleNextAgenda').then(function(recordsets){
						recordsets = recordsets[0];
						if(recordsets.length > 0){
							console.log("today list")
							$.sendMessage('Your List for Today: ');
							var todays = '';
							for(var i = 0; i < recordsets.length; i++){
								var room = recordsets[i].Room == null ? 'GSLC' : recordsets[i].Room;
								todays += (recordsets[i].Shift + ' | ' + room + ' | ' + recordsets[i].CourseName + ' - ' + recordsets[i].Campus)+"\n";
							}
							
							$.sendMessage(todays);

						}else{
							$.sendMessage('Your day seems bright! :)')
						}

					}).catch(function(err){
						console.log(err)
					});	
				}).catch(function(err){
					console.log(err)
				});	
		 	});
		}, function(){
			console.log("failed in checking");
		})
	}

	weeklyHandler($){
	var u = new Util.Util()

		u.cek($, function(user) {
		 	sql.connect(Util.CONNECTION_STRING_BM).then(function() {
		 		new sql.Request()
		 		.input('UserID' , sql.VarChar(10) , user.EmployeeID)
				.execute('bn_Student_GetActiveAcadCareer').then(function(recordsets){
					recordsets = recordsets[0];
					var ACAD_CAREER = recordsets[0] == null ? 'RS1' : recordsets[0].ACAD_CAREER;

					new sql.Request()
					.input('AcadCareer' , sql.VarChar(4) , ACAD_CAREER)
					.input('BinusID' , sql.VarChar(10) , user.EmployeeID)
					.execute('bn_Schedule_GetPeriods').then(function(recordsets){
						recordsets = recordsets[0];
						var STRM = recordsets[0] == null ? '1610' : recordsets[0].STRM;

						new sql.Request()
						.input('UserID' , sql.VarChar(16) , user.BinusianID)
						.input('AcadCareer' , sql.VarChar(4) , ACAD_CAREER)
						.input('STRM' , sql.VarChar(4) , STRM)
						.execute('bn_Student_GetMasterSchedule').then(function(recordsets){
							recordsets = recordsets[0];
							if(recordsets.length > 0){
								var weekly = [];
								//var arr = new Array(recordsets);
								var curr = [];
								var schedule = new Array();
								for(var i = 0; i < recordsets.length; i++){
									schedule[i] = new Array();
									schedule[i].push(recordsets[i].Day, recordsets[i].Date, recordsets[i].Room, recordsets[i].Course, recordsets[i].Campus, recordsets[i].StartTime, recordsets[i].EndTime);
								}

								for(var j=0; j < recordsets.length; j++){
									function compare(a,b) {
									  if (a < b)
									    return -1;
									  if (a > b)
									    return 1;
									  return 0;
									}

									schedule.sort(compare(schedule[j][0], schedule[j][0]));

									var start = new Date(schedule[j][5]);
									var end = new Date(schedule[j][6]);
									var shift = (start.getHours() + ':' + start.getMinutes() + ' - ' + end.getHours() + ':' + ("0" + end.getMinutes()).substr(-2));
									weekly += (schedule[j][1] + ' | ' + shift + ' | ' + schedule[j][2] + ' | ' + schedule[j][3] + ' - ' + schedule[j][4])+"\n";
								}

								$.sendMessage('Your List for This Week: ');
								$.sendMessage(weekly);

							}else{
								$.sendMessage('No Course Available')
							}

						}).catch(function(err){
							console.log(err)
						});	
					}).catch(function(err){
						console.log(err)
					});	
				}).catch(function(err){
					console.log(err)
				});	
		 	});
		}, function(){
			console.log("failed in checking");
		})
	}

	newsHandler($){
	var u = new Util.Util()

		u.cek($, function(user) {
		 	sql.connect(Util.CONNECTION_STRING_BM).then(function() {
		 		var Page = "1";
				var NewsType = "1";
		 		new sql.Request()
				.input('StudentID' , sql.VarChar(10) , user.BinusianID)
				.input('RoleID' , sql.VarChar(4) , user.RoleID)
				.input('Page' , sql.VarChar(10) , Page)
				.input('NewsType' , sql.VarChar(4) , NewsType)
				.execute('Student_GetNewsAndEvents').then(function(recordsets){
					recordsets = recordsets[0];
					
					if(recordsets.length > 0){
						var news = '';
						for(var i = 0; i < recordsets.length; i++){
							news += recordsets[i].Title + ': ' + recordsets[i].Description +"\n";
						}
						$.sendMessage(news,{ parse_mode: 'HTML' });

					}else{
						$.sendMessage('No News Available')
					}

				}).catch(function(err){
					console.log(err)
				});
			});
		}, function(){
			console.log("failed in checking");
		})
	}	


	get routes(){
		return{
			'course' : 'courseHandler',
			'gpa' : 'gpaHandler',
			'today' : 'todayHandler',
			'weekly' : 'weeklyHandler',
			'news' : 'newsHandler'
		}
	}
}

exports.DashboardController = DashboardController;