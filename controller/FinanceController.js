'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
const Util = require('../Util.js');


class FinanceController extends TelegramBaseController {
	financeHandler($){
		var u = new Util.Util()

		u.cek($, function(user) {
			sql.connect(Util.CONNECTION_STRING_BM).then(function() {
				var priod = "";
				var kdsem = "";
				var tglawal = "";
				var tglakhir = "";
				
		 		new sql.Request()
		 		.input('priod' , sql.VarChar(4) , priod)
		 		.input('kdsem' , sql.VarChar(1) , kdsem)
		 		.input('nim' , sql.VarChar(10) , user.BinusianID)
		 		.input('tglawal' , sql.VarChar(10) , tglawal)
		 		.input('tglakhir' , sql.VarChar(10) , tglakhir)
				.execute('sprBLMSLoadTotalFinanceSummary').then(function(recordsets){
					recordsets = recordsets[0];
					for(var i=0; i<recordsets.length; i++)
					{
						var charge = recordsets[i].charge;
						var payment = recordsets[i].payment;
						var deposit = recordsets[i].deposit;
						var summary = '';

						for(var i = 0; i < recordsets.length; i++){
							summary += ('Charge     : ' + recordsets[i].charge + '\nPayment : ' + recordsets[i].payment + '\nDeposit    : ' + recordsets[i].deposit)+"\n\n";
						}
					//	$.sendMessage(summary);

						new sql.Request()
				 		.input('BinusianID' , sql.VarChar(10) , user.EmployeeID)
						.execute('bn_Financial_Student_GetFinancialSummary').then(function(recordsets){
							recordsets = recordsets[0];

							if(recordsets.length > 0){
								var financial = '';

								for(var i = 0; i < recordsets.length; i++){
									financial += (recordsets[i].item_term + ' - ' + recordsets[i].descr + ' (' + recordsets[i].descr_ind +') \nDue Date: ' + String(recordsets[i].due_dt).substring(0,10)  + ' | Charge: ' + recordsets[i].item_amt + ' | Payment: ' + recordsets[i].applied_amt) + "\n\n";
								}
								$.sendMessage(summary + financial);
							}

						}).catch(function(err) {
							console.log(err)
						});

					}

				}).catch(function(err) {
					console.log(err)
				});
			});
		}, function(){
			console.log('check error');
		});	
	}
	get routes(){
		return {
			'finance' : 'financeHandler',
		}
	}
}

exports.FinanceController = FinanceController;