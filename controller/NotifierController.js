'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
const Util = require('../Util.js');
var tgAPI;
var message;

class NotifierController extends TelegramBaseController {
    constructor(tg, msg) {
        super();
        tgAPI = tg;
        message = msg;
    }
    run() {
        //var msg = $._message._text.substring($._message._text.indexOf(' ')+1);
        sql.connect(Util.CONNECTION_STRING_TELEGRAM).then(function() {
            new sql.Request().query('SELECT ChatID FROM MsUser WHERE ChatID IS NOT NULL').then(function(recordset) {
                for(var i=0; i<recordset.length; i++)
                {
                    tgAPI.api.sendMessage(recordset[i].ChatID, message);
                }
                 //$.sendMessage('Message sent to '+recordset.length+' user(s)');
                 console.log("Message: "+message+" broadcasted to "+recordset.length+" users");
            }).catch(function(err) {
	            console.log(err);
	        });
        }).catch(function(err) {
            console.log(err);
        });
    }
}

exports.NotifierController = NotifierController;