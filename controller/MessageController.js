'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
const Util = require('../Util.js');


class MessageController extends TelegramBaseController {
	messageHandler($){
		var u = new Util.Util()

		u.cek($, function(user) {
		 	sql.connect(Util.CONNECTION_STRING_MESSAGE_BM).then(function() {
		 		new sql.Request()
				.input('BinusID' , sql.VarChar(10) , user.EmployeeID)
				.input('RoleID' , sql.VarChar(4) , user.SpecificRoleID)
				.execute('bn_GetInbox').then(function(recordsets){
					recordsets = recordsets[0];

					if (recordsets.length > 0) {

						//while (recordsets.length <= 10) {
							var messages = '';
							for(var i = 0; i < recordsets.length; i++){
								if (recordsets[i].IsRead == 0) {
									messages += (recordsets[i].Sender.substring(recordsets[i].Sender.search("-")+1) + '\n' + recordsets[i].Subject + ' - ' + recordsets[i].ReceivedDate + " [NEW]")+"\n";
								} else {
									messages += (recordsets[i].Sender.substring(recordsets[i].Sender.search("-")+1) + '\n' + recordsets[i].Subject + ' - ' + recordsets[i].ReceivedDate)+"\n";
								}
								if(i == 9)
									break;
							}
							$.sendMessage(messages);
						//}
					}else{
						$.sendMessage('No Messages Available')
					}

				}).catch(function(err){
					console.log(err)
				});	
			}).catch(function(err){
				console.log(err)
			});
		}, function(){
			console.log("failed in checking");
		})
		
	}

	get routes(){
		return{
			'message' : 'messageHandler'
		}
	}
}

exports.MessageController = MessageController;