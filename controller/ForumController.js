'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
const sql3 = require('mssql');
const Util = require('../Util.js');
var msg = '';

// getcourse
// 	untuk setiap course,getclass
// 		untuk setiap class, getthread
//$.sendMessage("NO  THREAD TITLE                        LAST POST                      STATUS\n1      Thead di Indo\nBy: FERICO SAMUEL      last reply: 12/10/2016 08:23     Reply :1");

class ForumController extends TelegramBaseController {
	forumHandler($){
		var self = this;
		var u = new Util.Util()

		u.cek($, function(user) {
			sql.connect(Util.CONNECTION_STRING_BM).then(function() {
		 		new sql.Request()
		 		.input('UserID' , sql.VarChar(10) , user.EmployeeID)
				.execute('bn_Student_GetActiveAcadCareer').then(function(recordsets){
					recordsets = recordsets[0];
					if(recordsets == null || recordsets.length == 0) {
						$.sendMessage("No Forum Data");
						return;
					}
					var ACAD_CAREER = recordsets[0] == null ? 'RS1' : recordsets[0].ACAD_CAREER;

					new sql.Request()
					.input('AcadCareer' , sql.VarChar(4) , ACAD_CAREER)
					.input('BinusID' , sql.VarChar(11) , user.EmployeeID)
					.execute('bn_Schedule_GetPeriods').then(function(recordsets){
						recordsets = recordsets[0];
						var STRM = recordsets[0] == null ? '1610' : recordsets[0].STRM;

						sql3.connect(Util.CONNECTION_STRING_FORUM_BM).then(function() {
							new sql3.Request()
					 		.input('StudentID' , sql.VarChar(11) , user.EmployeeID)
					 		.input('LecturerID' , sql.VarChar(11) , '')
					 		.input('AcadCareer' , sql.VarChar(25) , ACAD_CAREER)
					 		.input('Strm' , sql.VarChar(4) , STRM)
							.execute('Forum_GetCourse').then(function(recordsetsCourse){
								recordsetsCourse = recordsetsCourse[0];
								self.getClass(recordsetsCourse, user, ACAD_CAREER, STRM, $, 0, false);
							}).catch(function(err) {
								console.log(err)
							});
						});

					}).catch(function(err) {
						console.log(err)
					});

				}).catch(function(err) {
					console.log(err)
				});	
			});
		}, function(){
			console.log('check error');
		});	
	}

	getClass(course, user, ACAD_CAREER, STRM, $, courseCount, isSent) {
		var self = this;
		if(course[courseCount] == null || course[courseCount].ID == null) {
			if((courseCount + 1) < course.length)
			{
				self.getClass(course, user, ACAD_CAREER, STRM, $, ++courseCount, isSent);
				return;
			}
			else if(!isSent && courseCount == course.length)
				$.sendMessage("No Forum Data");
		}
		new sql3.Request()
 		.input('ExternalSystemID' , sql.VarChar(16) , user.EmployeeID)
 		.input('KdDsn' , sql.VarChar(11) , '')
 		.input('AcadCareer' , sql.VarChar(4) , ACAD_CAREER)
 		.input('Strm' , sql.VarChar(4) , STRM)
 		.input('CrseID' , sql.VarChar(6) , course[courseCount].ID)
		.execute('Forum_GetClass').then(function(recordsetsClass){
			recordsetsClass = recordsetsClass[0];
			
			var newSent = self.getThread(recordsetsClass, course[courseCount], user, ACAD_CAREER, STRM, $, 0, false);
			isSent = isSent == true ? isSent : newSent;

			if((courseCount + 1) < course.length)
			{
				self.getClass(course, user, ACAD_CAREER, STRM, $, ++courseCount, isSent);
				return;
			}
			else if(!isSent && courseCount == course.length)
				$.sendMessage("No Forum Data");
		}).catch(function(err){
			console.log(err)
		});
	}

	getThread(c, course, user, ACAD_CAREER, STRM, $, count, isMessageSent) {
		var self = this;
		new sql3.Request()
		.input('BinusID' , sql.VarChar(11) , user.EmployeeID)
		.input('ForumTypeID' , sql.VarChar(2) , "1")
		.input('AcadCareer' , sql.VarChar(4) , ACAD_CAREER)
		.input('Strm' , sql.VarChar(4) , STRM)
		.input('ClassSection' , sql.VarChar(8) , c[count].ID)
		.input('CrseID' , sql.VarChar(6) , course.ID)
		.execute('Forum_GetThread').then(function(recordsets){
			recordsets = recordsets[0];

			if (recordsets.length > 0) {
				var forum = '';

				for(var k=0; k<recordsets.length; k++)
				{
					forum += ([k+1]+". " + recordsets[k].ForumThreadTitle + '\nBy: ' + recordsets[k].creator + '  Last Reply: ' + (recordsets[k].lastreply == null ? '-' : recordsets[k].lastreply)  + '  Reply: ' + recordsets[k].replies) + "\n\n";
				}

				$.sendMessage('Forum list for Course: ' + course.Caption+"\n\n"+forum);

				if( (count+1) < c.length)
				{
					self.getThread(c, course, user, ACAD_CAREER, STRM, $, ++count, true);
					return true;
				}
			}
			else if( (count+1) < c.length)
			{
				self.getThread(c, course, user, ACAD_CAREER, STRM, $, ++count, false);
				return false;
			}
			else
				return false;

		}).catch(function(err){
			console.log(err)
		});
	}


	get routes(){
		return{
			'forum' : 'forumHandler',
		}
	}
}

exports.ForumController = ForumController;