'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
const Util = require('../Util.js');


class GslcController extends TelegramBaseController {
	gslcHandler($){
		var u = new Util.Util()

		u.cek($, function(user) {
		 	sql.connect(Util.CONNECTION_STRING_BM).then(function() {
		 		new sql.Request()
		 		.input('BinusID' , sql.VarChar(11) , user.EmployeeID)
		 		.input('SpecificRole' , sql.Int, user.SpecificRoleID)
				.execute('bn_calendar_classSchedule_GetStudentClassSchedule').then(function(recordsets){
					recordsets = recordsets[0];
					
					if(recordsets.length > 0){
						var gslcCounter = 0;
						var currentDate = new Date();
						currentDate.setHours(0);
						currentDate.setMinutes(0);
						currentDate.setSeconds(0);
						currentDate.setMilliseconds(0);
						var gslc = '';

						for(var i = 0; i < recordsets.length; i++){
							var s = Date.parse(recordsets[i].START_DT);
							if (recordsets[i].N_DELIVERY_MODE == "GSLC" && s >=  currentDate) {
								gslc += recordsets[i].COURSE_TITLE_LONG + ' - ' + recordsets[i].CLASS_SECTION + ' - ' + recordsets[i].START_DT.getDate() + '/' + (recordsets[i].START_DT.getMonth()+1) + '/' + recordsets[i].START_DT.getFullYear() + ' (' + recordsets[i].MEETING_TIME_START + ' - ' + recordsets[i].MEETING_TIME_END + ")" + "\n";
								if(++gslcCounter == 3)
									break;
							}
						}
						$.sendMessage(gslc);

					}else{
						$.sendMessage('No Upcoming GSLC');
					}

				}).catch(function(err){
					console.log(err)
				});	

		 	});

		}, function(){
			console.log("failed in checking");
		})

	}	


	get routes(){
		return{
			'gslc' : 'gslcHandler',
		}
	}
}

exports.GslcController = GslcController;