'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
var Util = require('../Util.js');

class PingController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    pingHandler($) {
        var u = new Util.Util();
        u.cek($, function() {
            $.sendMessage('halo '+$._message._from._firstName, true)
        }, function(){
            console.log('failed; incomplete registration')
            //$.sendMessage('halo')
        })
        

        // $.runMenu({
        //     message: 'Please send your phone number:',
        //     options: {
        //         parse_mode: 'Markdown',
        //         oneTimeKeyboard:true
        //     },
        //     oneTimeKeyboard:true,
        //     'Send My Phone Number': ($) => {
        //         console.log($);
        //         $.sendMessage('test hide', true);
        //     },
        //     'anyMatch': ($) => {
        //         //console.log($);
        //         $.sendMessage('test hide', true);
                
        //     }
        // })
    }
    get routes() {
        return {
            'ping': 'pingHandler',
        }
    }
}

exports.PingController = PingController;