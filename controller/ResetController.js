'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
var Util = require('../Util.js');
const sql = require('mssql');

class ResetController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    resetHandler($) {
        var u = new Util.Util();
        //u.cek($, function(user) {
            sql.connect(Util.CONNECTION_STRING_TELEGRAM).then(function() {
                new sql.Request()
                .input('TelegramID' , sql.VarChar(10) , $._message._from._id)
                .execute('bn_Telegram_ResetUser').then(function(recordsets){
                    $.sendMessage("Your Registration Data Has Been Reset! Send /start to get started");
                }).catch(function(err){
                    console.log(err)
                }); 
            }).catch(function(err){
                console.log(err)
            }); 
        // }, function(){
        //     console.log('failed; incomplete registration')
        //     //$.sendMessage('halo')
        // })
    }
    get routes() {
        return {
            'reset': 'resetHandler',
        }
    }
}

exports.ResetController = ResetController;