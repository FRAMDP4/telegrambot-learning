'use strict'
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const sql = require('mssql');
var Util = require('../Util.js');

class StartController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */

    startHandler($) {
        var u = new Util.Util();
        u.cek($, function() {
            console.log('success')
            $.sendMessage('halo '+$._message._from._firstName, true)
        }, function(){
            console.log('failed')
            //$.sendMessage('halo')
        })
    }

    get routes() {
        return {
            'start': 'startHandler'
        }
    }
}

exports.StartController = StartController;